package com.common.Interface;

import java.util.LinkedHashMap;
import java.util.Map;

public class App implements Interface12, Interface1 {

	private static boolean polindrome(String in){
		if(in == null) 
			return false;
		StringBuilder strBuild = new StringBuilder(in);
		strBuild.reverse();
		System.out.println(in +" <> "+strBuild);
		System.out.println(in.compareTo("popa"));
		return strBuild.toString().equals(in);
	}
	
	//================================================
	private static String RomanNumerals(int value){
		
		LinkedHashMap<String, Integer> roman_numerals = new LinkedHashMap<String,Integer>();
		roman_numerals.put("M", 1000);
	    roman_numerals.put("CM", 900);
	    roman_numerals.put("D", 500);
	    roman_numerals.put("CD", 400);
	    roman_numerals.put("C", 100);
	    roman_numerals.put("XC", 90);
	    roman_numerals.put("L", 50);
	    roman_numerals.put("XL", 40);
	    roman_numerals.put("X", 10);
	    roman_numerals.put("IX", 9);
	    roman_numerals.put("V", 5);
	    roman_numerals.put("IV", 4);
	    roman_numerals.put("I", 1);
	    String res = "";
	    
	    for (Map.Entry<String, Integer> entry : roman_numerals.entrySet()){
	    	int matches = value/entry.getValue();
	        res += repeat(entry.getKey(), matches);
	        value = value % entry.getValue();
	    }
		
		return res;
	}
	
	public static String repeat(String s, int n){
		if(s == null) {
	        return null;
	    }
	    final StringBuilder sb = new StringBuilder();
	    for(int i = 0; i < n; i++) {
	        sb.append(s);
	    }
	    return sb.toString();
	}
	//=========================================================
	
	public static void main( String[] args )
    {		
//        System.err.println(polindrome("popi"));
        System.err.println(RomanNumerals(12));
    }

	public String method1() {
		// TODO Auto-generated method stub
		return null;
	}

	public Double method3() {
		// TODO Auto-generated method stub
		return null;
	}

	public String method2() {
		// TODO Auto-generated method stub
		return null;
	}

	public String method12() {
		// TODO Auto-generated method stub
		return null;
	}

}
