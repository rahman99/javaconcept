package com.common.email;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendingEmail {

	public static void main(String[] args) {
		String host = "smtp.gmail.com";
		String from ="r4.man181@gmail.com";
		String password="pass";
		String messages = "test random message";
		String to[] = {"rahmancommon@gmail.com"};
		Properties props = System.getProperties();
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.user", from);
		props.put("mail.smtp.password", password);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", 587);

		Session session = Session.getDefaultInstance(props, null);
		MimeMessage message = new MimeMessage(session);

		try {
			message.setFrom(new InternetAddress(from));
			InternetAddress[] toAddress = new InternetAddress[to.length];
			for(int i=0;i<to.length; i++){
				toAddress[i]=new InternetAddress(to[i]);
			}
			for(int i=0;i<toAddress.length;i++){
				message.addRecipient(RecipientType.TO, toAddress[i]);
			}
			message.setSubject("Testing Subject");
			message.setText(messages);
			
			Transport transport = session.getTransport("smtp");
			transport.connect(host, from, password);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();

			System.out.println("Done"); 

		} catch (MessagingException e) {
			System.out.println("failed sending email");
			throw new RuntimeException(e);
		}
	}

}
