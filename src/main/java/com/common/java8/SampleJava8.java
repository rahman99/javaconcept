package com.common.java8;

import java.util.ArrayList;
import java.util.List;

public class SampleJava8 {

	public static void main(String[] arg){
		
	}
	/**
	 * feature 1: method reference
	 */
	//////method reference//////////////////
	private static void getMethodReference(){
		List<String> names=new ArrayList<String>();
		names.add("Mahesh");
	    names.add("Suresh");
	    names.add("Ramesh");
	    names.add("Naresh");
	    names.add("Kalpesh");
	    
	    names.forEach(System.out::println);
	}
	//////end method reference//////////////
	
}
