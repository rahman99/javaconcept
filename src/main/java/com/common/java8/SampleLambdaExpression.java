package com.common.java8;

import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.BinaryOperator;

public class SampleLambdaExpression {
/**
 * lambda expression syntax:
 * parameter -> expression body
 * 
 * 
 */
	public static void main(String[] arg){
		// bentuk 1
		Runnable noArguments = () -> System.out.println("Hello World");
		noArguments.run();
		
		// bentuk 2
		OneArgument oneArg = argument -> "test "+argument;
		System.out.println(oneArg.getOneArgument("oneArgument"));
		
		// bentuk 3
		// jika expression body harus di pisahkan dengan ;
		// maka expression body harus di bungkus dalam { }
		Runnable multiStatement = () -> { 
											System.out.print("Hello1");
											System.out.println(" World1");
										};
		multiStatement.run();								
		
		// bentuk 4
		BinaryOperator<Long> add = (x, y) -> x + y;
				
		// bentuk 5
		BinaryOperator<Long> addExplicit = (Long x, Long y) -> x + y; //5
	}
	
	interface OneArgument{
		String getOneArgument(String one);
	}
	
	private void otherSampleLambda(){
		//sample 1
		//Old way:
		new Thread(new Runnable() {
		    @Override
		    public void run() {
		        System.out.println("Hello from thread");
		    }
		}).start();
		 
		//New way:
		new Thread(
		    () -> System.out.println("Hello from thread")
		).start();
		
		
		/*================================================*/
		// sample 2
		//Old way:
		Button button=new Button("button");
		button.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
		        System.out.println("The button was clicked using old fashion code!");
		    }
		});
		 
		//New way:
		button.addActionListener( (e) -> {
		        System.out.println("The button was clicked. From lambda expressions !");
		});
	}
}
