package com.common.java8;

/**
 * feature 1: default method
 */

public class SampleDefaultMethod implements Vehicle, Motor{

	public static void main(String[] x){
		SampleDefaultMethod s = new SampleDefaultMethod();
		s.print();
		System.out.println("============");
		s.printDefault();
		
		Greating greating = message -> message;
		String out = greating.printGreating("halo greating");
		System.out.println(out);
	}

	public void print() {
		Vehicle.writeStatic();
		Vehicle.super.printDefault();
		Motor.writeStatic();
	}
	
	//interface in a class
	interface Greating{
		String printGreating(String message);
	}
}

interface Vehicle{
	void print();
	static void writeStatic(){
		System.err.println("write vehicle");
	}
	default void printDefault() {
		System.err.println("default print2 vehicle");
	}
}

interface Motor{
	static void writeStatic(){
		System.err.println("write motor");
	}
}