package com.common.searching;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SplitPerSentance {

	public static void main(String[] args) {
//		readFile("/media/rahman/DATA/test.txt");
		readFile("/media/rahman/DATA/docs");
	}
	
	private static void readFile(String path){
		BufferedReader reader;
		String input;
		try {
			reader = new BufferedReader(new FileReader(path));
			while((input = reader.readLine()) != null){
				List<String> strList = splitSentences(input);
				String[] str1Array = null;
				for(String in : strList){
					str1Array = in.split(" ");
					for(byte i=0;i<str1Array.length;i++){
					if(i+1<str1Array.length)
						System.err.print("("+i+")"+str1Array[i]+" "+str1Array[i+1]+" ");
					}
					System.out.println();
				}
//				System.out.println();
			}			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static List<String> splitSentences(String source){
		List<String> listout = new ArrayList<>();
		BreakIterator iterator = BreakIterator.getSentenceInstance(Locale.US);
//		String source = "This is a test. This is a T.L.A. test. Now with a Dr. in it.";
		iterator.setText(source);
		int start = iterator.first();
		for (int end = iterator.next();
		    end != BreakIterator.DONE;
		    start = end, end = iterator.next()) {
				listout.add(source.substring(start, end));
//			  System.out.println(source.substring(start,end));
			}
		return listout;
	}
	
	

}
