package com.common.searching;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatchWordSeacrh {

	public static void main(String[] arg){
//		matchingWord();
		matchingWithPattern("ayam");
	}
	
	private static void matchingWord(){
		String testStr = "saya makan satekambing";
		String lookUp = "ate";
		
		if(testStr.indexOf(lookUp)!=-1){
			System.out.println("found");
		} else {
			System.err.println("not found");
		}
		
	}
	
	private static void matchingWithPattern(String key){		
		List<String> words = new ArrayList<>();
		words.add("nasi ayama kecap");
		words.add("adu jago #ayam kampung");
		words.add("soto harus Aayam sehat");
		
		Pattern pattern = Pattern.compile("(\\s|^)ayam(\\s|$)", Pattern.CASE_INSENSITIVE);
		
		for(String str : words){
			Matcher match = pattern.matcher(str);
			System.out.println(match.find());
		}
	}
}
