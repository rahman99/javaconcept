package com.common.searching;

public class DetechUniqueString {

	public static void main(String[] arg){
		String str = "rahmen its";
		boolean check = checkUnique(str);
		System.out.println(check);
	}
	
	private static boolean checkUnique(String str){
		boolean result = false;
		for(char s : str.toCharArray()){
			if(str.indexOf(s) == str.lastIndexOf(s)){
				result = true;
			} else {
				result = false;
			}
		}
		
		return result;
	}
}
