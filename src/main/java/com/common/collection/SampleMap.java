package com.common.collection;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class SampleMap {
	
	public static void main(String[] arg){
		treeMapSample();
	}
	/**
	 * HashMap akan menghasilkan ouput yang acak, 
	 * LinkedHashMap akan menghasilkan output berdasarkan urutan map.put
	 * TreeMap akan menghasilkan output sesuai dengan sorting key
	 */
	private static void treeMapSample(){
		Map<String, Object> tree = new LinkedHashMap<String, Object>();
		tree.put("sepuluh", 10);
		tree.put("sembilan belas", 19);
		tree.put("lima belas", 15);
		tree.put("tiga satu", 31);
		tree.put("dua empat", 24);
		tree.put("sebelas", 11);
		
		System.err.println(tree.values());
	}

}
