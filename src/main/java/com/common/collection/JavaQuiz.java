package com.common.collection;

import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class JavaQuiz {

	public static void main(String[] args) {
//		quiz1();
//		quiz2(10, 20, 5);
//		detectDuplicate("saya saya minum teh susu dan kopi susu".split(" "));
//		verargs(2, "saya", "rahman");
				
	}
	
	private static void quiz1(){
		Set<Short> set = new HashSet<>();
		for(short i=0;i<10;i++){
			set.add(i);
			set.remove(i-1);
		}
		System.out.println(set);
	}
	
	private static void quiz2(int... in){
		int total = 0;
		for(int x : in)
			total += x;
		System.err.println(total);
	}
	
	private static void quiz3(){
		
	}
	
	/**
	 * (String... input) disebut sebagai varargs yang mana di perlakukan seperti array
	 */
	private static void detectDuplicate(String... input){
		Set<String> nonduplicate = new HashSet<>();
		Set<String> duplicate = new HashSet<>();
//		String[] inputt = input.split(" ");		
		for(String collect : input)
			if(!nonduplicate.add(collect))
				duplicate.add(collect);			
			
		nonduplicate.removeAll(duplicate);
		
		System.err.println("nonduplicate : "+ nonduplicate);
		System.err.println("duplicate: "+duplicate);
	}
	
	private static void verargs(int x, String... arg){
		for(String s : arg)
			System.out.println(s);
		
		System.err.println(arg[arg.length-x]);
	}

}
