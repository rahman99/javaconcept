package com.common.collection;

import java.util.PriorityQueue;

public class PriorityqueueSample {

	public static void main(String[] args) {
		String[] array = {"one", "two", "three", "four", "five"};
		PriorityQueue<String> queue = new PriorityQueue<>();
		for(String str : array){
			queue.offer(str);
		}
		while(queue.size()>0){
			queue.remove();
		}
		System.err.println(queue.size());
	}

}
